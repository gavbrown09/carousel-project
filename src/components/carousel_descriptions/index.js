const ImgDescriptions = [
    "<h2 class='carousel-txt-title'>Range Rover – Exterior</h2><p class='carousel-txt-description'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sed tellus non lacus sagittis fringilla.</p>",
    "<h2 class='carousel-txt-title'>Range Rover – Interior</h2><p class='carousel-txt-description'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sed tellus non lacus sagittis fringilla.</p>",
    "<h2 class='carousel-txt-title'>Smart Remote</h2><p class='carousel-txt-description'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sed tellus non lacus sagittis fringilla.</p>",
    "<h2 class='carousel-txt-title'>AI Robot</h2><p class='carousel-txt-description'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sed tellus non lacus sagittis fringilla.</p>",
    "<h2 class='carousel-txt-title'>Smart Watch</h2><p class='carousel-txt-description'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sed tellus non lacus sagittis fringilla.</p>"
];

export default ImgDescriptions;
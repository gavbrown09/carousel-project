import React from 'react';
import { Col } from 'react-bootstrap';

/**
 * Carousel slider for displaying the next image.
 * 
 * @param path 
 * @returns 
 */
const CarouselSliderNext = ({ path }) => {
    const styles = {
        backgroundImage: `url(${path})`,
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        height: 400,
        width: 250,
        marginLeft: -300,
        alignSelf: 'center',
        zIndex: 998
    }
    return (
        <Col className="carousel-slider right-preview" style={styles}>
            <div className="carousel-preview-overlay overlay-right"></div>
        </Col>
    );
}

export default CarouselSliderNext;
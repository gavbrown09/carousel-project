import React, { useState } from 'react';
import {
    Container,
    Row,
    Col
} from 'react-bootstrap';
import CarouselSlider from '../carousel_slider';
import CarouselSliderNext from '../carousel_slider/nextSlide';
import CarouselSliderPrev from '../carousel_slider/prevSlide';
import Arrows from '../carousel_arrows';
import imgPath from '../carousel_images';
import ImgDescriptions from '../carousel_descriptions/index';

/**
 * Carousel main component
 * 
 * @returns 
 */
function Carousel() {
    var [currentImage, setCurrentImage] = useState(0);
    const [nextImagePreview, setNextImagePreview] = useState(1);
    const [prevImagePreview, setPrevImagePreview] = useState(imgPath.length -1);
    const [currentDescription, setCurrentDescription] = useState(0);

    
    {/* 
        Previous slide button
        Set image previews based on current slide
    */}
    const prevSlide = () => {
        const lastImage = imgPath.length -1;
        const resetImage = currentImage === 0;
        const resetPrevImage = currentImage === 1;
        const index = resetImage ? lastImage : currentImage -1;
        const resetIndexNext = resetImage ? 0 : currentImage +1;
        const resetIndexPrev = resetPrevImage ? lastImage : currentImage -1;

        if (resetImage) {
            setNextImagePreview(resetIndexNext)
        } else {
            setNextImagePreview(index +1)
        }

        if (resetPrevImage) {
            setPrevImagePreview(resetIndexPrev)
        } else {
            setPrevImagePreview(index -1);
        }

        setCurrentImage(index);
        setCurrentDescription(index);
    }

    {/* 
        Next slide button
        Set image previews based on current slide
    */}
    const nextSlide = () => {
        const lastImage = imgPath.length -1;
        const resetImage = currentImage === lastImage;
        const resetPrevImage = currentImage === 4;
        const resetNextImage = currentImage === 3;
        const index = resetImage ? 0 : currentImage +1;
        const resetIndexPrev = resetPrevImage ? lastImage : currentImage -1;
        const resetIndexNext = resetNextImage ? 0 : currentImage +1;

        if (resetNextImage) {
            setNextImagePreview(resetIndexNext);
        } else {
            setNextImagePreview(index +1);
        }

        if (resetPrevImage) {
            setPrevImagePreview(resetIndexPrev);
        } else {
            setPrevImagePreview(index -1);
        }

        setCurrentImage(index);
        setCurrentDescription(index);
    }

    return (
        <Container>
            <Row className="justify-content-center align-self-center carousel m-auto">
                <CarouselSliderPrev path={ imgPath[prevImagePreview] } />
                <Col className="carousel">
                    <Arrows 
                        direction="left"
                        click={prevSlide}
                        icon={<i className="fas fa-chevron-left fa-lg"></i>}
                    />
                    <Arrows 
                        direction="right"
                        click={nextSlide}
                        icon={<i className="fas fa-chevron-right fa-lg"></i>}
                    />
                    <CarouselSlider path={ imgPath[currentImage] } description={ ImgDescriptions[currentDescription] } />
                </Col>
                <CarouselSliderNext path={ imgPath[nextImagePreview] } />
            </Row>
        </Container>
    );    
}

export default Carousel;
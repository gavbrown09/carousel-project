import './App.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './components/header';
import Showcase from './components/showcase';
import Carousel from './components/carousel';

function App() {
  return (
    <div className="App">
      <Header />
      <Showcase />
      <Carousel />
    </div>
  );
}

export default App;

import React from 'react';
import {
    Container
} from 'react-bootstrap';

function Showcase() {
    return (
        <Container className="text-center py-5">
            <h2 className="color-jade">Showcase</h2>
            <p>
                We help innovate product brands launch in low-cost high footfall retail, all fully managed.
            </p>
            <p>
                Check out our showcase reel for examples on how we can represent your products, boosting your business growth.
            </p>
        </Container>
    );
}

export default Showcase;
/**
 * Arrow component for scrolling through slides.
 * 
 * @param direction
 * @param click
 * @param icon 
 * @returns 
 */
const Arrows = ({ direction, click, icon }) => {
    return (
        <div className={ `carousel-arrow ${direction}` }
            onClick={ click }>
            { icon }
        </div>
    );
}

export default Arrows;
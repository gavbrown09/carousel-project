import React from 'react';
import {
    Container
} from 'react-bootstrap';
import Logo from '../../assets/images/situ-logo.svg';

function Header() {
    return (
        <Container className="brand-logo">
            <img src={Logo} alt="Logo" />
        </Container>
    );
}

export default Header;
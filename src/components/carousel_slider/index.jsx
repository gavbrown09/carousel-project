import React from 'react';

/**
 * Carousel slider for displaying main image.
 * 
 * @param path
 * @param description 
 * @returns 
 */
const CarouselSlider = ({ path, description }) => {
    const styles = {
        backgroundImage: `url(${path})`,
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        height: 500,
        boxShadow: '0px 0px 11px 1px rgb(0 0 0 / 92%)'
    }
    return (
        <div className="carousel-slider" style={styles}>
            <div className="carousel-description" dangerouslySetInnerHTML={{__html: description}} />
        </div>
    );
}

export default CarouselSlider;
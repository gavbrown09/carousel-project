import React from 'react';
import { Col } from 'react-bootstrap';

/**
 * Carousel slider for displaying the previous image.
 * 
 * @param path 
 * @returns
 */
const CarouselSliderPrev = ({ path }) => {
    const styles = {
        backgroundImage: `url(${path})`,
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        height: 400,
        width: 250,
        alignSelf: 'center',
        zIndex: 998
    }
    return (
        <Col className="carousel-slider left-preview" style={styles}>
            <div className="carousel-preview-overlay overlay-left"></div>
        </Col>
    );
}

export default CarouselSliderPrev;